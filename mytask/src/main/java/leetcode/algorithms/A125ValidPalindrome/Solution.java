package leetcode.algorithms.A125ValidPalindrome;

/**
 https://leetcode.com/problems/valid-palindrome/description/
 Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

 Note: For the purpose of this problem, we define empty string as valid palindrome.

 Example 1:

 Input: "A man, a plan, a canal: Panama"
 Output: true
 Example 2:

 Input: "race a car"
 Output: false
 */
public class Solution {
    public boolean isPalindrome(String s) {
        int start = 0, end = s.length() - 1;
        while (start < end) {
            char startChar = Character.toLowerCase(s.charAt(start));
            char endChar = Character.toLowerCase(s.charAt(end));

            if (!Character.isLetterOrDigit(startChar)) {
                start++;
                continue;
            }

            if (!Character.isLetterOrDigit(endChar)) {
                end--;
                continue;
            }

            if (startChar != endChar) {
                return false;
            }
            start++;
            end--;
        }

        return true;
    }

    /**
     * does not work because it considers all symbols including spaces, but in the task - take plcae only
     * alfanumeric symbols
     * @param s
     * @return
     */
    public boolean isPalidrom2(String s) {
        char[] chars = s.toCharArray();
        int length = chars.length;
        for (int i = 0; i < length / 2; i++) {
            if (chars[i] != chars[length - 1 - i]) {
                return false;
            }
        }
        return true;
    }
}
