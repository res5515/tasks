package leetcode.algorithms.A155MinStack;

import leetcode.algorithms.common.ListNode;
import leetcode.algorithms.common.ListNodeFull;

/*
https://leetcode.com/problems/min-stack/description/

Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.
Example:
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin();   --> Returns -3.
minStack.pop();
minStack.top();      --> Returns 0.
minStack.getMin();   --> Returns -2.
 */
public class MinStack {
    ListNode nodeTop;

    /** initialize your data structure here. */
    public MinStack() {

    }

    public void push(int x) {
        if (nodeTop == null) {
            nodeTop = new ListNode(x);
            nodeTop.next = null;
        } else {
            ListNode temp =  new ListNode(x);
            temp.next = nodeTop;
            nodeTop = temp;
        }
    }

    public void pop() {
        if (nodeTop != null) {
            nodeTop = nodeTop.next;
        }
    }

    public int top() {
        if (nodeTop != null) {
            return nodeTop.val;
        } else {
            return 0;
        }
    }

    public int getMin() {
        if (nodeTop == null) {
            return 0;
        }
        ListNode temp = nodeTop.next;
        int min = nodeTop.val;
        while (temp != null) {
            if (min > temp.val) {
                min = temp.val;
            }
            temp = temp.next;
        }
        return min;
    }
}
