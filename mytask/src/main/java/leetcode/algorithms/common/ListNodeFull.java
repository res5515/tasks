package leetcode.algorithms.common;

public class ListNodeFull {
    public int val;
    public ListNodeFull next;
    public ListNodeFull prev;

    public ListNodeFull(int x) {
        val = x;
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }
}
