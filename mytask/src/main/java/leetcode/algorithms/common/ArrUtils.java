package leetcode.algorithms.common;

public class ArrUtils {
    public static void printArr(int[] arr){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            stringBuilder.append(arr[i] + ",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() -1);
        System.out.println(stringBuilder);
    }

    public static void printArr(int[] arr, int length){
        if (length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < length; i++) {
                stringBuilder.append(arr[i] + ",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() -1);
            System.out.println(stringBuilder);
        }
    }
}
