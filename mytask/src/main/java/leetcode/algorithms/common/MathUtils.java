package leetcode.algorithms.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MathUtils {
    public static Integer[] expandNumber(int n) {
        List<Integer> numbersList;
        if (n < 10) {
            numbersList = Arrays.asList(n);
        } else {
            numbersList = new ArrayList<>();
            while (n >= 10) {
                numbersList.add(n % 10);
                n /= 10;
            }
            numbersList.add(n);
        }

        Integer[] numbersArr = new Integer[numbersList.size()];
        Collections.reverse(numbersList);
        numbersList.toArray(numbersArr);
        return numbersArr;
    }
}
