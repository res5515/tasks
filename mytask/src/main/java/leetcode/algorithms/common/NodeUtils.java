package leetcode.algorithms.common;

import java.util.LinkedList;
import java.util.List;

public class NodeUtils {
    public static void printListNodes(ListNode node){
        if (node == null) {
            System.out.println("null");
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (node != null) {
            stringBuilder.append(node.val + " ");
            node = node.next;
        }
        stringBuilder.deleteCharAt(stringBuilder.length() -1);
        System.out.println(stringBuilder);
    }

    public static void print(ListNode node) {
        if (node == null) {
            System.out.println("null");
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (node != null) {
            stringBuilder.append(node + " ");
            node = node.next;
        }
        stringBuilder.deleteCharAt(stringBuilder.length() -1);
        System.out.println(stringBuilder);
    }

    public static int[] serializeListNode(ListNode node) {
        List<Integer> list = new LinkedList();
        while (node != null) {
            list.add(node.val);
            node = node.next;
        }
        return list.stream().mapToInt(Integer::intValue).toArray();
    }

    public static ListNode deserialiseListNode(int[] arr) {
        if (arr.length == 0) return null;

        ListNode firstNode = new ListNode(arr[0]);
        ListNode node = null;
        node = firstNode;
        for (int i = 1; i < arr.length; i++) {
            node.next = new ListNode(arr[i]);
            node = node.next;

        }
        return firstNode;
    }
}
