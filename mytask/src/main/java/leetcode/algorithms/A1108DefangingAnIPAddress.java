package leetcode.algorithms;

/*
https://leetcode.com/problems/defanging-an-ip-address/
Given a valid (IPv4) IP address, return a defanged version of that IP address.

A defanged IP address replaces every period "." with "[.]".



Example 1:

Input: address = "1.1.1.1"
Output: "1[.]1[.]1[.]1"
Example 2:

Input: address = "255.100.50.0"
Output: "255[.]100[.]50[.]0"


Constraints:

The given address is a valid IPv4 address.
 */
public class A1108DefangingAnIPAddress {
    public String defangIPaddr(String address) {
        char[] result = new char[address.length() + 6];
        int k = 0;
        for (int i = 0; i < address.length(); i++) {
            if (address.charAt(i) == '.') {
                result[k++] = '[';
                result[k++] = '.';
                result[k++] = ']';
            } else {
                result[k++] = address.charAt(i);
            }
        }

        return new String(result);
    }
}
