package leetcode.algorithms.a344reversestring;

/**
 * Created by nayil on 27.10.17.
 */
public class SolutionDima {
    public String reverseString(String s) {
        char[] chars = s.toCharArray();
        int left = 0, right = chars.length - 1;
        while (left < right) {
            char temp = chars[left];
            chars[left++] = chars[right];
            chars[right--] = temp;
        }
        return new String(chars);
    }
}
