package leetcode.algorithms.a344reversestring;

/**
 * Created by nayil on 27.10.17.
 */
public class Solution {
    public String reverseString(String s) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length/2; i++) {
            char ch = chars[chars.length - 1 -i];
            chars[chars.length - 1 -i] = chars[i];
            chars[i] = ch;
        }
        return new String(chars);
    }
}
