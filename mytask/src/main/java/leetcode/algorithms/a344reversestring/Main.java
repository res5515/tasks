package leetcode.algorithms.a344reversestring;

/**
 *  https://leetcode.com/problems/reverse-string

  Write a function that takes a string as input and returns the string reversed.

  Example:
  Given s = "hello", return "olleh".
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverseString("hello"));
    }
}
