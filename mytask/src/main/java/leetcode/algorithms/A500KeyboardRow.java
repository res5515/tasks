package leetcode.algorithms;

//https://leetcode.com/problems/keyboard-row/description/
//Given a List of words1, return the words1 that can be typed using letters of alphabet on only one row's of American keyboard like the image below.
//Input: ["Hello", "Alaska", "Dad", "Peace"]
//        Output: ["Alaska", "Dad"]
public class A500KeyboardRow {
    public static void main(String[] args) {
        String[] input = {"Hello", "Alaska", "Dad", "Peace"};
        checkResult(new A500KeyboardRowSolution(), input);
        System.out.println("----------------------");
        checkResult(new A500KeyboardRowDima(), input);
    }

    private static void checkResult(A500KeyboardRowInterface solution, String[] input){
        String[] words2 = solution.findWords(input);
        if(words2.length != 0){
            for(String word : words2){
                System.out.println(word);
            }
        }else {
            System.out.println("matches not found");
        }
    }
}
