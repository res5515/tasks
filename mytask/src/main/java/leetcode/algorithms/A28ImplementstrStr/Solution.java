package leetcode.algorithms.A28ImplementstrStr;

/**
 Implement strStr().

 Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

 Example 1:

 Input: haystack = "hello", needle = "ll"
 Output: 2
 Example 2:

 Input: haystack = "aaaaa", needle = "bba"
 Output: -1
 Clarification:

 What should we return when needle is an empty string? This is a great question to ask during an interview.

 For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().
*/

public class Solution {
    public int strStr(String haystack, String needle) {
        if (needle.isEmpty()
                || haystack.isEmpty() && needle.isEmpty()
                || haystack.length() < needle.length()
                || haystack.length() == needle.length() && haystack.equals(needle)){
            return 0;
        }
        char[] charsHaystack = haystack.toCharArray();
        char[] charsNeedle = needle.toCharArray();
        boolean notFound = true;
        int index = -1;
        for (int i = 0; i < charsHaystack.length; i++) {
            if (charsHaystack[i] == charsNeedle[0]){
                if (charsHaystack.length - i >= charsNeedle.length) {
                    for (int j = 1; j < charsNeedle.length; j++) {
                        if (charsHaystack[i+j] != charsNeedle[j]) {
                            notFound = false;
                            break;
                        }
                    }
                    if (!notFound) {
                        index = i;
                        break;
                    }
                    notFound = true;
                }
            }
        }
        return index;
    }

    public int strStr2(String haystack, String needle) {
        if (needle.isEmpty()
                || haystack.isEmpty() && needle.isEmpty()
                || haystack.length() == needle.length() && haystack.equals(needle)){
            return 0;
        }
        if (haystack.length() < needle.length()) {
            return -1;
        }

        char[] charsHaystack = haystack.toCharArray();
        char[] charsNeedle = needle.toCharArray();
        int haystackLenght = haystack.length();
        int needleLength = needle.length();
        for (int i = 0; i < charsHaystack.length; i++) {
            if (haystackLenght -i < needleLength) {
                return -1;
            }

            boolean found = checkSubStr(i, charsHaystack, charsNeedle);

            if (found) {
                return i;
            }
        }
        return -1;
    }

    private boolean checkSubStr(int start, char[] charsHaystack, char[] charsNeedle) {
        for (int j = 0; j < charsNeedle.length; j++) {
            if (charsHaystack[start + j] != charsNeedle[j]) {
                return false;
            }
        }
        return true;
    }
}
