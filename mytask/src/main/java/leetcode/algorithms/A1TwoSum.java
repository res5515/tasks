package leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

//Given an array of integers, return indices of the two numsbers such that they add up to a specific target.
//You may assume that each input would have exactly one solution, and you may not use the same element twice.
//Example:
//        Given numss = [2, 7, 11, 15], target = 9,
//
//        Because numss[0] + numss[1] = 2 + 7 = 9,
//        return [0, 1].
public class A1TwoSum {
    public static void main(String[] args) {
        int[] nums = {2,7,11,15,4,8};
        int target = 15;
        int[] result;
         result = solve1(nums, target);
        System.out.print("indices [" + result[0] + ", " + result[1] + "]");
        System.out.println("\n------------------------------");
        result = solve1(nums, target);
        System.out.print("indices [" + result[0] + ", " + result[1] + "]");

    }

    //bad decision
//    o(n^2)
    private static int[] solve1(int[] nums, int target){
        int tempsum = 0, n = -1, m = -1;
        int res[] = {-1, -1};
        for(int i = 0; i < nums.length - 1; i++){
            for(int j = i + 1; j < nums.length; j++){
                tempsum = nums[i] + nums[j];
                if (tempsum == target){
                    n = i; m = j;
                    res[0] = i; res[1] = j;
                    return res ;
                }
            }
        }
        return res;
    }

    //good decision
    //o(n)
    private static int[] solve2(int[] nums, int target){
        int temp = 0;
        Integer n;
        int res[] = {-1, -1};
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for(int i = 0; i < nums.length; i++){
            temp = target - nums[i];
            n = map.get(temp);
            if(n != null){
                res[0] = n; res[1] = i;
                break;
            }else{
                map.put(nums[i], i);
            }
        }
        return res;
    }
}
