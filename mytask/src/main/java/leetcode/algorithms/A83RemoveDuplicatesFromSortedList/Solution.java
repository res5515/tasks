package leetcode.algorithms.A83RemoveDuplicatesFromSortedList;

/**
 https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/
 Given a sorted linked list, delete all duplicates such that each element appear only once.

 Example 1:

 Input: 1->1->2
 Output: 1->2
 Example 2:

 Input: 1->1->2->3->3
 Output: 1->2->3
 */

import leetcode.algorithms.common.ListNode;

public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) return head;
        ListNode currentNode = head, nextNode = head.next;
        while (currentNode != null && nextNode != null) {
            if (currentNode.val == nextNode.val) {
                currentNode.next = nextNode.next;
                nextNode = currentNode.next;
            } else {
                currentNode = currentNode.next;
                nextNode = nextNode.next;
            }
        }
        return head;
    }

    public ListNode deleteDuplicatesDima(ListNode head) {
        if(head == null || head.next == null)
            return head;

        ListNode current = head;
        while( current != null && current.next != null){
            if(current.val == current.next.val){
                current.next = current.next.next;
            }else{
                current = current.next;
            }
        }
        return head;
    }
}
