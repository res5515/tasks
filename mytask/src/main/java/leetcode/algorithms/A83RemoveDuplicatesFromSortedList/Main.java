package leetcode.algorithms.A83RemoveDuplicatesFromSortedList;

import static leetcode.algorithms.common.NodeUtils.deserialiseListNode;
import static leetcode.algorithms.common.NodeUtils.print;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr1 = {1,1,2},
                arr2 = {1,1},
                arr3 = {1,1,2,3,3},
                arr4 = {};
        print(solution.deleteDuplicates(deserialiseListNode(arr1)));
        print(solution.deleteDuplicates(deserialiseListNode(arr2)));
        print(solution.deleteDuplicates(deserialiseListNode(arr3)));
        print(solution.deleteDuplicates(deserialiseListNode(arr4)));
    }
}
