package leetcode.algorithms.A20ValidParentheses;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Solution {
    public boolean isValid(String s) {
        Set<Character> openBracket = new HashSet<>(Arrays.asList('(','[','{'));
        Map<Character, Character> brackets = new HashMap<Character, Character>(){
            {
                put('(',')');
                put('[',']');
                put('{','}');
            }
        };
        Deque<Character> deque = new ArrayDeque<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (openBracket.contains(chars[i])) {
                deque.add(chars[i]);
            } else {
                Character bracket = deque.pollLast();
                if (bracket == null || brackets.get(bracket) != chars[i]) {
                    return false;
                }
            }
        }

        if (deque.size() == 0) return true;
        else return false;
    }
}
