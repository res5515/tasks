package leetcode.algorithms.A14LongestComonPrefix;

import java.util.Arrays;

/**
 * the same as Solution3 but using the array of chars
 */
public class Solution3 {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        if (strs.length == 1) return strs[0];

        int minLength = strs[0].length();
        int minLengthIndex = 0;
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() < minLength) {
                minLength = strs[i].length();
                minLengthIndex = i;
            }
        }
        String commonPrefix = strs[minLengthIndex];
        char[] result = new char[minLength];
        int realCharAmount = 0;

        for (int i = 0; i < commonPrefix.length(); i++) {
            for (int j = 0; j < strs.length; j++) {
                if (!strs[j].startsWith(new String(Arrays.copyOf(result, realCharAmount)) + commonPrefix.charAt(i))) {
                    return new String(Arrays.copyOf(result, realCharAmount));
                }
            }

            result[i] = commonPrefix.charAt(i);
            realCharAmount++;
        }
        return new String(result);
    }
}
