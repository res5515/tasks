package leetcode.algorithms.A14LongestComonPrefix;

public class Solution {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        if (strs.length == 1) return strs[0];
        StringBuilder result = new StringBuilder();
        String firstWord = strs[0];

        for (int i = 0; i < firstWord.length(); i++) {
            for (int j = 1; j < strs.length; j++) {
                if (!strs[j].startsWith(result.toString() + firstWord.charAt(i))) {
                    return result.toString();
                }
            }

            result.append(firstWord.charAt(i));
        }
        return result.toString();
    }
}
