package leetcode.algorithms.A14LongestComonPrefix;

public class Solution4 {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        if (strs.length == 1) return strs[0];

        String minLeghtWord = findMinLengthWord(strs);
        if (minLeghtWord.length() == 0) {
            return "";
        }

        int low = 0;
        int high = minLeghtWord.length();
        boolean isFound;
        String temp;
        String result = "";
        while (low < high) {
            int middle = (high - low) / 2;
            int pivot = low + (middle == 0 ? 1 : middle);
            temp = minLeghtWord.substring(0, pivot);
            isFound = true;
            for (int j = 0; j < strs.length; j++) {
                if (!strs[j].startsWith(temp)) {
                    isFound = false;
                    break;
                }
            }

            if (isFound) {
                low = pivot;
                result = temp;
            } else {
                high = pivot == high ? pivot - 1 : pivot;
            }
        }


        return result;
    }

    private static String findMinLengthWord(String[] strs) {
        int minLength = strs[0].length();
        int minLengthIndex = 0;
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() < minLength) {
                minLength = strs[i].length();
                minLengthIndex = i;
            }
        }
        return strs[minLengthIndex];
    }
}
