package leetcode.algorithms.A14LongestComonPrefix;

public class Solution2 {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        if (strs.length == 1) return strs[0];
        StringBuilder result = new StringBuilder();

        int minLength = strs[0].length();
        int minLengthIndex = 0;
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() < minLength) {
                minLength = strs[i].length();
                minLengthIndex = i;
            }
        }
        String commonPrefix = strs[minLengthIndex];

        for (int i = 0; i < commonPrefix.length(); i++) {
            for (int j = 0; j < strs.length; j++) {
                if (!strs[j].startsWith(result.toString() + commonPrefix.charAt(i))) {
                    return result.toString();
                }
            }

            result.append(commonPrefix.charAt(i));
        }
        return result.toString();
    }
}
