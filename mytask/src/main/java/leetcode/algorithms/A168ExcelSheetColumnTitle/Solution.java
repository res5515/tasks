package leetcode.algorithms.A168ExcelSheetColumnTitle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
https://leetcode.com/problems/excel-sheet-column-title/
Given a positive integer, return its corresponding column title as appear in an Excel sheet.

For example:

    1 -> A
    2 -> B
    3 -> C
    ...
    26 -> Z
    27 -> AA
    28 -> AB
    ...
Example 1:

Input: 1
Output: "A"
Example 2:

Input: 28
Output: "AB"
Example 3:

Input: 701
Output: "ZY"
 */
public class Solution {
    Map<Integer, Character> map = initMap();
    public String convertToTitle(int n) {
        if (n <= 0) {
            return "";
        }
        Map<Integer, Character> map = initMap();

        if (n <= 26) {
            return map.get(n).toString();
        }

        StringBuilder stringBuilder = new StringBuilder();
        int k = n;
        int remainderOfDivision;
        List<Integer> result = new ArrayList<>();
        while (k > 26) {
            remainderOfDivision = k % 26;
            k = k / 26;
            if (remainderOfDivision != 0) {
                stringBuilder.append(map.get(remainderOfDivision));
            } else {
                stringBuilder.append('Z');
                k--;
            }
        }
        stringBuilder.append(map.get(k));

        return stringBuilder.reverse().toString();
    }

    private Map<Integer, Character> initMap() {
        Map<Integer, Character> map = new HashMap<>();
        map.put(1, 'A');
        map.put(2, 'B');
        map.put(3, 'C');
        map.put(4, 'D');
        map.put(5, 'E');
        map.put(6, 'F');
        map.put(7, 'G');
        map.put(8, 'H');
        map.put(9, 'I');
        map.put(10, 'J');
        map.put(11, 'K');
        map.put(12, 'L');
        map.put(13, 'M');
        map.put(14, 'N');
        map.put(15, 'O');
        map.put(16, 'P');
        map.put(17, 'Q');
        map.put(18, 'R');
        map.put(19, 'S');
        map.put(20, 'T');
        map.put(21, 'U');
        map.put(22, 'V');
        map.put(23, 'W');
        map.put(24, 'X');
        map.put(25, 'Y');
        map.put(26, 'Z');
        return map;
    }
}
