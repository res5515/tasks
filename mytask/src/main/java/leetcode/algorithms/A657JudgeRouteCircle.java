package leetcode.algorithms;

/**
 * Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle,
 * which means it moves back to the original place.
 * The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are
 * R (Right), L (Left), U (Up) and D (down). The output should be true or false representing whether the robot makes
 * a circle.
 Example 1:
        Input: "UD"
        Output: true
 Example 2:
        Input: "LL"
        Output: false
 */
public class A657JudgeRouteCircle {
    public static void main(String[] args) {
        String moves = "ULDR";
        A657JudgeRouteCircleSolution worker = new A657JudgeRouteCircleSolution();
        System.out.println(worker.judgeCircle(moves));
        System.out.println(worker.judgeCircleDima(moves));
    }
}
