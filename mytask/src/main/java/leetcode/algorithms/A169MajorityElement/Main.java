package leetcode.algorithms.A169MajorityElement;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = {2,2,1,1,1,2,2};
        System.out.println(solution.majorityElement(nums));
    }
}
