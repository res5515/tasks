package leetcode.algorithms.A169MajorityElement;

/*
    https://leetcode.com/problems/majority-element/description/
    Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

Example 1:

Input: [3,2,3]
Output: 3
Example 2:

Input: [2,2,1,1,1,2,2]
Output: 2
 */

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public int majorityElement(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }

        Map<Integer, Integer> map = new HashMap<>();
        int majorityAmount = nums.length / 2;
        for (int i = 0; i < nums.length; i++) {
            Integer count = map.get(nums[i]);
            if (count != null) {
                if (++count > majorityAmount) {
                    return nums[i];
                } else {
                    map.put(nums[i], count);
                }
            } else {
                map.put(nums[i], 1);
            }
        }
        return 0;
    }
}
