package leetcode.algorithms.A141LinkedListCycle;

import leetcode.algorithms.common.ListNode;
import leetcode.algorithms.common.NodeUtils;

public class Main {
    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        ListNode l1 = NodeUtils.deserialiseListNode(arr1);
//        l1.next.next.next.next.next = l1;
        l1.next = l1;
        Solution solution = new Solution();
        System.out.println(solution.hasCycle(l1));
    }
}
