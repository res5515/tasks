package leetcode.algorithms.A141LinkedListCycle;

import leetcode.algorithms.common.ListNode;

import java.util.HashSet;
import java.util.Set;

/*
https://leetcode.com/problems/linked-list-cycle/description/
Given a linked list, determine if it has a cycle in it.

Follow up:
Can you solve it without using extra space?
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode slow = head;
        ListNode fast = head.next;
        while (slow != fast) {
            if (fast == null || fast.next == null) {
                return false;
            }
            slow = slow.next;
            fast = fast.next.next;
        }
        return true;
    }

    //using extra space
    public boolean hasCycle2(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        ListNode p1 = head;
        while (p1 != null) {
            if (!set.add(p1)) {
                return true;
            }
            p1 = p1.next;
        }
        return false;
    }

    //bad solution O(n*n)
    /*
    I suppose it does not work
     */
    public boolean hasCycle3(ListNode head) {
        if (head == null || head.next == null) return false;
        ListNode p1 = head, p2;
        while (p1 != null) {
            p2 = p1.next;
            while (p2 != null) {
                if (p1 == p2) {
                    return true;
                }
                p2 = p2.next;
            }
            p1 = p1.next;
        }
        return false;
    }
}
