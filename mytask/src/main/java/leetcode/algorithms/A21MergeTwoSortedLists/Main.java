package leetcode.algorithms.A21MergeTwoSortedLists;

import leetcode.algorithms.common.ListNode;

import static leetcode.algorithms.common.NodeUtils.deserialiseListNode;
import static leetcode.algorithms.common.NodeUtils.printListNodes;
import static leetcode.algorithms.common.NodeUtils.print;


/**
 Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together
 the nodes of the first two lists.

 Example:

 Input: 1->2->4, 1->3->4
 Output: 1->1->2->3->4->4

 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();

        ListNode nodes1 = deserialiseListNode(new int[]{1,3,6});
        ListNode nodes2 = deserialiseListNode(new int[]{2,4,6});

        ListNode result = solution.mergeTwoLists(nodes1, nodes2);
        printListNodes(result);
        System.out.println("----------------------");
        print(result);
    }
}
