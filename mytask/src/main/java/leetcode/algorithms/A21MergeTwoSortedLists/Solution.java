package leetcode.algorithms.A21MergeTwoSortedLists;

import leetcode.algorithms.common.ListNode;

import javax.annotation.Nonnull;

public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode current, first;

        //check that some list is null
        if (l1 == null) {
            return l2;
        }

        if (l2 == null) {
            return l1;
        }

        //find the first element to return
        // here l1 != null and l2 != null
        if (l1.val <= l2.val) {
            first = l1;
            current = l1;
            l1 = l1.next;
        } else {
            first = l2;
            current = l2;
            l2 = l2.next;
        }

        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                current.next = l1;
                l1 = l1.next;
            } else {
                current.next = l2;
                l2 = l2.next;
            }
            current = current.next;
        }

        addRemaining(l1, current);
        addRemaining(l2, current);

        return first;
    }

    private void addRemaining(ListNode listNode, ListNode current) {
        while (listNode != null) {
            current.next = listNode;
            current = current.next;
            listNode = listNode.next;
        }
    }
}
