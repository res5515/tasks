package leetcode.algorithms;

// https://leetcode.com/problems/add-two-numbers
//You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
//        You may assume the two numbers do not contain any leading zero, except the number 0 itself.
//        Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
//        Output: 7 -> 0 -> 8
public class AddTwoNumbers {
    public static void main(String[] args) {
        int[] arr1 = {2, 4, 3};
        int[] arr2 = {5, 6, 4};
        calcAndPrint(arr1, arr2);
        System.out.println("--------------------------");
        int[] arr3 = {2, 4, 3};
        int[] arr4 = {5, 6, 6};
        calcAndPrint(arr3, arr4);
        System.out.println("--------------------------");
        int[] arr5 = {0};
        int[] arr6 = {5, 6, 6};
        calcAndPrint(arr5, arr6);
        System.out.println("--------------------------");
        int[] arr7 = {0, 7};
        int[] arr8 = {5, 6, 6, 4, 5};
        calcAndPrint(arr7, arr8);
    }

    // O(n)
    public static ListNode addTwoNumbers1(ListNode l1, ListNode l2) {
        ListNode result = null, resultTemp = null, ltemp1, ltemp2;
        int valTemp1 = 0, valTemp2 = 0;
        boolean nextDigitIncrement = false;
        ltemp1 = l1;
        ltemp2 = l2;
        ListNode lnew;
        while (ltemp1 != null || ltemp2 != null) {
            if (ltemp1 != null) {
                valTemp1 = ltemp1.val;
                ltemp1 = ltemp1.next;
            } else {
                valTemp1 = 0;
            }
            if (ltemp2 != null) {
                valTemp2 = ltemp2.val;
                ltemp2 = ltemp2.next;
            } else {
                valTemp2 = 0;
            }

            int tempVal = nextDigitIncrement ? valTemp1 + valTemp2 + 1 : valTemp1 + valTemp2;
            if (tempVal >= 10) {
                tempVal = tempVal % 10;
                nextDigitIncrement = true;
            } else {
                nextDigitIncrement = false;
            }
            if (result == null) {
                result = new ListNode(tempVal);
                resultTemp = result;
            } else {
                lnew = new ListNode(tempVal);
                resultTemp.next = lnew;
                resultTemp = lnew;
            }
            //if no next while operation and need to encrement next digit - do it manually
            if (ltemp1 == null && ltemp2 == null && nextDigitIncrement) {
                lnew = new ListNode(1);
                resultTemp.next = lnew;
            }
        }
        return result;
    }

    private static ListNode init(int[] arr) {
        ListNode listNode = null;
        ListNode ltemp = listNode;
        for (int i = 0; i < arr.length; i++) {
            if (listNode == null) {
                listNode = new ListNode(arr[0]);
                ltemp = listNode;
            } else {
                ListNode lnew = new ListNode(arr[i]);
                ltemp.next = lnew;
                ltemp = lnew;
            }
        }
        return listNode;
    }

    private static void print(ListNode listNode) {
        if (listNode == null) {
            return;
        }
        StringBuilder stringBuilder = new StringBuilder(listNode.val + "");
        ListNode ltemp = listNode.next;
        while (ltemp != null) {
            stringBuilder.append(" -> " + ltemp.val);
            ltemp = ltemp.next;
        }
        System.out.println(stringBuilder.toString());
    }

    private static void calcAndPrint(int[] arr1, int[] arr2) {
        ListNode listNode1 = init(arr1);
        print(listNode1);
//        System.out.println("------------------");
        ListNode listNode2 = init(arr2);
        print(listNode2);
        System.out.println("sum:");
        ListNode result = addTwoNumbers1(listNode1, listNode2);
        print(result);
    }

    //given linked list (from task)
    private static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
