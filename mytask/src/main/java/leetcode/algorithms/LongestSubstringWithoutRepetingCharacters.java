package leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

/**
 https://leetcode.com/problems/longest-substring-without-repeating-characters

 Given a string, find the length of the longest substring without repeating characters.
 Examples:
 Given "abcabcbb", the answer is "abc", which the length is 3.
 Given "bbbbb", the answer is "b", with the length of 1.
 Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */
public class LongestSubstringWithoutRepetingCharacters {
    public static void main(String[] args) {
        String s1 = "abcbabb";
        System.out.println(lengthOfLongestSubstring(s1));
        System.out.println("------------------------------");
        s1 ="bbbb";
        System.out.println(lengthOfLongestSubstring(s1));
        System.out.println("------------------------------");
        s1 ="pwewrwkwqwkew";
        System.out.println(lengthOfLongestSubstring(s1));
        System.out.println("-------------Dima-----------------");
        System.out.println(lengthOfLongestSubstringDima("abcbbbdceb"));

    }
    private static String lengthOfLongestSubstring(String s){
        String result = "";
        StringBuilder strBuilder = new StringBuilder();
        Map<Character, Integer> wordMap;
        Map<String, Integer> resultMap = new HashMap<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); i++) {
            wordMap = new HashMap<>();
            for (int j = i; j < s.length(); j++) {
                if(wordMap.get(chars[j]) == null){
                    strBuilder.append(chars[j]);
                    wordMap.put(chars[j], j);
                }else{
                    break;
                }
            }
            resultMap.put(strBuilder.toString(), strBuilder.capacity());
            strBuilder.delete(0, strBuilder.capacity());
        }
        for(Map.Entry<String, Integer> entry : resultMap.entrySet()){
            if (result.length() < entry.getKey().length()) {
                result = entry.getKey();
            }
            System.out.println(entry.getKey());
        }
        System.out.println("------------------------------");
        return result;
    }

    public static int lengthOfLongestSubstringDima(String s) {
        if (s.length() == 0) return 0;
        int result = 0;
        int temp = 0;
        int[] bucket = new int[256];
        for (int i = 0, k = 0; k < s.length();) {
            if (bucket[s.charAt(k)] == 0) {
                bucket[s.charAt(k)]++;
                k++;
                temp++;
                result = Math.max(result, temp);
            } else {
                bucket[s.charAt(i)]--;
                temp--;
                i++;
            }
        }
        return result;
    }
}
