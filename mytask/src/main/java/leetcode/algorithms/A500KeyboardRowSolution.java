package leetcode.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nayil on 02.09.17.
 */
public class A500KeyboardRowSolution implements A500KeyboardRowInterface{
    public String[] findWords(String[] words) {
        String[] result = new String[words.length];
        String[] lines = {"qwertyuiopQWERTYUIOP", "asdfghjklASDFGHJKL", "zxcvbnmZXCVBNM"};
        Set<Character>[] linesSet = initArrSet(lines);
        int k = 0;

        for (int i = 0; i < words.length; i++) {
            int lineNumber = retrieveLineNumberSet(linesSet, words[i].charAt(0));
            if(lineNumber >= 0){
                if(lineContainWord(linesSet[lineNumber], words[i])){
                    result[k++] = words[i];
                }
            }

        }
        return Arrays.copyOf(result, k);
    }

    private int retrieveLineNumberSet(Set<Character>[] lines, char ch){
        for (int i = 0; i < lines.length; i++) {
            if(lines[i].contains(ch)) {
                return i;
            }
        }
        return -1;
    }

    private boolean lineContainWord(Set<Character> set, String word){
        char[] wordChars = word.toCharArray();
        boolean result = true;
        for (int i = 1; i < wordChars.length; i++) {
            if(!set.contains(wordChars[i])){
                result = false;
                break;
            }
        }
        return result;
    }

    private Set<Character>[] initArrSet(String[] lines){
        Set<Character>[] arrSet = new Set[lines.length];
        for (int i = 0; i < lines.length; i++) {
            char[] charsLine = lines[i].toCharArray();
            Set<Character> set = new HashSet<>();
            for (char c : charsLine){
                set.add(c);
            }
            arrSet[i] = set;
        }
        return arrSet;
    }
}
