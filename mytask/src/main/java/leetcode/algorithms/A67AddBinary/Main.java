package leetcode.algorithms.A67AddBinary;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.addBinary("11","10"));
        System.out.println(solution.addBinary("1010","1011"));
        System.out.println(solution.addBinary("10101","1011"));
        System.out.println(solution.addBinary("101011","10"));
        System.out.println(solution.addBinary("11","1"));
    }
}
