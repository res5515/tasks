package leetcode.algorithms.A67AddBinary;

/**
 * https://leetcode.com/problems/add-binary/
 Given two binary strings, return their sum (also a binary string).

 The input strings are both non-empty and contains only characters 1 or 0.

 Example 1:

 Input: a = "11", b = "1"
 Output: "100"
 Example 2:

 Input: a = "1010", b = "1011"
 Output: "10101"
 */
public class Solution {
    public String addBinary(String a, String b) {
        int aI = a.length() - 1;
        int bI = b.length() - 1;
        StringBuilder result = new StringBuilder();
        char nextValue = '0';
        while (aI >= 0 || bI >=0) {
            char aChar = aI >= 0 ? a.charAt(aI) : '0';
            char bChar = bI >= 0 ? b.charAt(bI) : '0';
            if (aChar == '1' && bChar == '1') {
                result.append(nextValue);
                nextValue = '1';
            } else if (nextValue == '1' && (aChar == '1' || bChar == '1')) {
                result.append('0');
                nextValue = '1';
            } else if (nextValue == '1') {
                result.append('1');
                nextValue = '0';
            } else if (aChar == '1' || bChar == '1') {
                result.append('1');
                nextValue = '0';
            } else {
                result.append('0');
                nextValue = '0';
            }
            aI--; bI--;
        }

        if (nextValue == '1') {
            result.append('1');
        }

        return result.reverse().toString();
    }
}
