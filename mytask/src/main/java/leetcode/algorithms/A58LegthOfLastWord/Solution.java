package leetcode.algorithms.A58LegthOfLastWord;

/**
 https://leetcode.com/problems/length-of-last-word/description/
 Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.

 If the last word does not exist, return 0.

 Note: A word is defined as a character sequence consists of non-space characters only.

 Example:

 Input: "Hello World"
 Output: 5
 */
public class Solution {
    public int lengthOfLastWord(String s) {
        int start = 0, end = s.length() - 1;
        boolean hasLastSpace = true;

        while (hasLastSpace && end >= 0) {
            if (s.charAt(end) == ' ') {
                end--;
            } else {
                hasLastSpace = false;
            }
        }

        for (int i = end; i >=0 ; i--) {
            if (s.charAt(i) == ' ') {
                start = i;
                break;
            }
        }

        return end == -1 ? 0: end - start;
    }

    public int lengthOfLastWordDima(String s) {
        int end = s.length() - 1;
        while (end >= 0 && s.charAt(end) == ' ') {
            end--;
        }
        int start = end;
        while (start >= 0 && s.charAt(start) != ' ') {
            start--;
        }
        return end - start;
    }
}
