package leetcode.algorithms.A35SearchInsertPosition;

/**
 * https://leetcode.com/problems/search-insert-position/
 * Given a sorted array and a target value, return the index if the target is found.
 * If not, return the index where it would be if it were inserted in order.
 *
 * You may assume no duplicates in the array.
 *
 * Example 1:
 *
 * Input: [1,3,5,6], 5
 * Output: 2
 * Example 2:
 *
 * Input: [1,3,5,6], 2
 * Output: 1
 * Example 3:
 *
 * Input: [1,3,5,6], 7
 * Output: 4
 * Example 4:
 *
 * Input: [1,3,5,6], 0
 * Output: 0
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.searchInsert(new int[]{0,1,2,3,4}, 0));
        System.out.println(solution.searchInsert(new int[]{0,1,2,4}, 2));
        System.out.println(solution.searchInsert(new int[]{0,1,2,4}, 3));
        System.out.println(solution.searchInsert(new int[]{0,1,2,3,4}, 7));
    }
}
