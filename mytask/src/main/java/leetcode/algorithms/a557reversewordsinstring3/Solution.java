package leetcode.algorithms.a557reversewordsinstring3;

/**
 * Created by nayil on 05.09.17.
 */
public class Solution implements SolutionInterface{
    public String reverseWords(String s) {
        char[] chars = s.toCharArray();
        char[] charsRes = new char[s.length()];
        int start = -1, len = 0;
        for (int i = 0; i < chars.length; i++) {
            if(chars[i] == ' '){
                len = i - (start + 1); //length of the word
                for (int j = start + 1; j < i; j++) {
                    charsRes[j] = chars[start + len--];
                }
                charsRes[i] = chars[i]; //add space
                start = i;
                continue;
            }
            if(i == chars.length -1){ //at the end of the string there is no space
                i++;
                len = i - (start + 1); //length of the word
                for (int j = start + 1; j < i; j++) {
                    charsRes[j] = chars[start + len--];
                }
            }
        }
        return new String(charsRes);
    }
}
