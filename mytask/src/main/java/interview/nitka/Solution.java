package interview.nitka;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Solution {
    /*
     * Calculate only half of matrix. Less memory - more difficult calculation in loops
     */
    public double w(int row, int pos) {
        if (row == 0) {
            return 0d;
        }

        Deque<List<Double>> allWeights = new ArrayDeque<>(row);
        addFirstWeight(allWeights);

        for (int i = 1; i < row; i++) {
            List<Double> rowWeights = new ArrayList<>((i / 2) + 1);
            for (int j = 0; j < (i / 2) + 1; j++) {
                rowWeights.add(calcWeight(j, allWeights.getLast()));
            }
            allWeights.addLast(rowWeights);
        }

        return calcWeight(pos < (row / 2) + 1 ? pos : row - pos,
                allWeights.getLast());
    }

    private void addFirstWeight(Deque<List<Double>> allWeights) {
        List<Double> weightFirst = new ArrayList<>(0);
        weightFirst.add(0d);
        allWeights.addLast(weightFirst);
    }

    private double calcWeight(int pos, List<Double> previousWeights) {
        if (pos == 0) {
            return (calcSingleWeight(previousWeights.get(0)));
        } else {
            if (previousWeights.size() == pos) {
                return calcSingleWeight(previousWeights.get(pos - 1)) * 2;
            } else {
                return (calcSingleWeight(previousWeights.get(pos - 1))
                        + calcSingleWeight(previousWeights.get(pos)));
            }
        }
    }

    private double calcSingleWeight(double upperWeight) {
        return (upperWeight + 1) / 2;
    }

//--------------------------------------------------------------
//-----------------other solutions------------------------------
//--------------------------------------------------------------

    /*
    Calculate full matrix. More memory - easy calculation in loops
     */
    public double w2(int row, int pos) {
        if (row == 0) {
            return 0d;
        }

        Deque<List<Double>> allWeights = new ArrayDeque<>(row);
        addFirstWeight(allWeights);

        for (int i = 1; i < row; i++) {
            List<Double> rowWeights = new ArrayList<>(row + 1);
            for (int j = 0; j < i + 1; j++) {
                rowWeights.add(calcWeight(i, j, allWeights.getLast()));
            }
            allWeights.addLast(rowWeights);
        }

        return calcWeight(row, pos, allWeights.getLast());
    }

    private double calcWeight(int row, int pos, List<Double> previousWeights) {
        if (pos == 0 || pos == row ) {
            return (calcSingleWeight(previousWeights.get(0)));
        } else {
            return (calcSingleWeight(previousWeights.get(pos - 1))
                    + calcSingleWeight(previousWeights.get(pos)));
        }
    }

    /*
    Bad solution using recursion.
     */
    public double w3(int row, int pos) {
        if (row == 0) {
            return 0d;
        }

        if (row == 1) {
            return 1/2d;
        }

        int weight = 1;

        if (pos == 0 || pos == row) {
            return  (w3(row - 1, 0) + weight) / 2;
        }

        return ((w3(row -1, pos -1) + weight)
                + (w3(row -1, pos ) + weight)) / 2;
    }
}
