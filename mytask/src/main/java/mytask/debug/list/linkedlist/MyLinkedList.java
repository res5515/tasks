package mytask.debug.list.linkedlist;

import com.sun.org.apache.bcel.internal.generic.RET;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

/**
 * Created by nayil on 06.07.17.
 */
public class MyLinkedList<E> implements List<E>{
    private int size;
    private Node<E> first;
    private Node<E> last;

    @Override
    public boolean add(E e) {
        if (size == 0){
            Node<E> newE = new Node<>(e, null, null);
            first = newE;
        }else{
            if (last == null){
                Node<E> newE = new Node<>(e, first, null);
                first.next = newE;
                last = newE;
            }else {
                Node<E> oldLast = last;
                Node<E> newE = new Node<>(e, oldLast, null);
                oldLast.next = newE;
                last = newE;
            }

        }
        size++;
        return true;
    }
    @Override
    public boolean remove(Object o) {
        for(Node<E> node = first; node != null; node = node.next){
            if (o.equals(node.e)){
                deleteNode(node);
                size--;
                return true;
            }
        }
        return false;
    }

    private E deleteNode(Node<E> node){
        // node != null
        E e = node.e;
        if (node.prev == null) { //this is the first element in collection
            first = node.next;
        }else if (node.next == null) { // node is the last element in collection
            Node<E> prevNode = node.prev;
            prevNode.next = null;
            last = prevNode;
        }else{
            Node<E> prevNode = node.prev;
            Node<E> nextNode = node.next;
            prevNode.next = nextNode;
            nextNode.prev = prevNode;
        }
        return e;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl<>();
    }

    private static class Node<E>{
        E e;
        Node<E> prev;
        Node<E> next;

        Node(E e, Node<E> prev, Node<E> next){
            this.e = e;
            this.prev = prev;
            this.next = next;
        }
    }

    private class IteratorImpl<E> implements Iterator<E>{
        Node<E> currentElement = (Node<E>) first;
        @Override
        public boolean hasNext() {
            if (currentElement != null){
                return true;
            }
            return false;
        }

        @Override
        public E next() {
            E e = currentElement.e;
            currentElement = currentElement.next;
            return e;
        }

        @Override
        public void remove() {

        }

        @Override
        public void forEachRemaining(Consumer<? super E> action) {

        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }



    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }




    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
