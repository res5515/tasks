package mytask.debug.list.linkedlist;

import java.util.LinkedList;

/**
 * Created by nayil on 04.07.17.
 */
public class LinkListRun {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
//        MyLinkedList<String> linkedList = new MyLinkedList<>();
        linkedList.add("1");
        linkedList.add("2");
        linkedList.add("3");
        linkedList.remove("2");
//        linkedList.contains();
//        linkedList.iterator();
        for(String s : linkedList){
            System.out.println(s);
        }
    }
}
