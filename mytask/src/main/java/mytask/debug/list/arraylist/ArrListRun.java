package mytask.debug.list.arraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nayil on 02.07.17.
 */
public class ArrListRun {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
//        MyArrList<String> list = new MyArrList<>();
        for (int i = 0; i < 11; i++) {
            list.add(new Integer(i).toString());
        }
//        list.get(3);
//        list.set(3, null);
        list.remove(3);
        list.remove("3");
        list.add(10, "10");
        list.iterator();
        System.out.println("------------------");
        Iterator<String> iter = list.iterator();
        for (String s : list){
            System.out.println(s);
        }
    }
}
