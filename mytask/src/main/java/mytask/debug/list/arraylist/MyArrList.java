package mytask.debug.list.arraylist;

import java.util.*;
import java.util.function.Consumer;

/**
 * Created by nayil on 02.07.17.
 */
public class MyArrList<E> implements List<E> {
    private int size;
    private static final int MIN_SIZE = 2;
    private E[] arrE;

    public MyArrList(){
        arrE = (E[])new Object[MIN_SIZE];
    }
    public MyArrList(int startSize){
        arrE = (E[]) new Object[startSize];
    }

    @Override
    public boolean add(E e) {
        enoughCapacity();
        arrE[size++] = e;
        return true;
    }

    @Override
    public E remove(int index) {
        if (index > this.size) throw new IndexOutOfBoundsException();
        E e = arrE[index];
        System.arraycopy(arrE,index + 1,arrE, index, size - index - 1);
        size--;
        return e;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null){
            for (int i = 0; i < size; i++) {
                if (arrE[i] == null){
                    remove(i);
                    return true;
                }
            }
        } else{
                for (int i = 0; i < size; i++) {
                    if(arrE[i].equals(o)){
                        remove(i);
                        return true;
                    }

            }
        }
        return false;
    }

    private void enoughCapacity(){
        if ((size + 1) > arrE.length){
            int oldLength = arrE.length;
            arrE = Arrays.copyOf(arrE, arrE.length + (oldLength >> 1));
        }
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0){
            return true;
        }
        return false;
    }


    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl<>();
    }

    private class IteratorImpl<E> implements Iterator<E>{
        private int k;
        @Override
        public boolean hasNext() {
            if (k < size){
                return true;
            }
            return false;
        }

        @Override
        public E next() {
            if(k > size) throw new NoSuchElementException("");
            E e = (E) arrE[k++];
            return e;
        }

        @Override
        public void remove() {
            if(k > size) throw new NoSuchElementException("");
            arrE[size - 1] = null;
        }

        @Override
        public void forEachRemaining(Consumer<? super E> action) {

        }
    }
    @Override
    public boolean contains(Object o) {
        return false;
    }


    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }


    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
