package A168ExcelSheetColumnTitle;

import leetcode.algorithms.A168ExcelSheetColumnTitle.Solution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {
    Solution solution = new Solution();

    @Test
    public void tst() {
        String s1 = solution.convertToTitle(1);
        assertEquals("A", s1);

        s1 = solution.convertToTitle(2);
        assertEquals("B", s1);

        s1 = solution.convertToTitle(3);
        assertEquals("C", s1);

        s1 = solution.convertToTitle(26);
        assertEquals("Z", s1);

        s1 = solution.convertToTitle(28);
        assertEquals("AB", s1);

        s1 = solution.convertToTitle(701);
        assertEquals("ZY", s1);

        s1 = solution.convertToTitle(52);
        assertEquals("AZ", s1);
    }
}
