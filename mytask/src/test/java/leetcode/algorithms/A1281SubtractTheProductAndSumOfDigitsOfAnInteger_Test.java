package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1281SubtractTheProductAndSumOfDigitsOfAnInteger_Test {
    @Test
    public void solutionTest() {
        A1281SubtractTheProductAndSumOfDigitsOfAnInteger solution = new A1281SubtractTheProductAndSumOfDigitsOfAnInteger();
        assertEquals(15, solution.subtractProductAndSum(234));
    }
}
