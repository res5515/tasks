package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1221SplitAStringInBalancedStrings_Test {
    @Test
    public void solutionTest() {
        A1221SplitAStringInBalancedStrings solution = new A1221SplitAStringInBalancedStrings();
        assertEquals(4, solution.balancedStringSplit("RLRRLLRLRL"));
        assertEquals(3, solution.balancedStringSplit("RLLLLRRRLR"));
        assertEquals(2, solution.balancedStringSplit("RLRRRLLRLL"));
    }
}
