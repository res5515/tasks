package leetcode.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1431KidsWithTheGreatestNNumberOfCandies_Test {
    @Test
    public void solutionTest() {
        A1431KidsWithTheGreatestNNumberOfCandies a1431KidsWithTheGreatestNNumberOfCandies =  new A1431KidsWithTheGreatestNNumberOfCandies();
        Assertions.assertArrayEquals(new Boolean[]{true,true,true,false,true},
                a1431KidsWithTheGreatestNNumberOfCandies.kidsWithCandies(new int[]{2,3,5,1,3}, 3).toArray());
        Assertions.assertArrayEquals(new Boolean[]{true,false,false,false,false},
                a1431KidsWithTheGreatestNNumberOfCandies.kidsWithCandies(new int[]{4,2,1,1,2}, 1).toArray());
    }
}
