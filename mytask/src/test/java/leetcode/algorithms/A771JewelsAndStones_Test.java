package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A771JewelsAndStones_Test {
    @Test
    public void solutionTest() {
        A771JewelsAndStones a771JewelsAndStones = new A771JewelsAndStones();
        assertEquals(3, a771JewelsAndStones.numJewelsInStones("aA", "aAAbbbb"));
        assertEquals(0, a771JewelsAndStones.numJewelsInStones("z", "ZZ"));
    }
}
