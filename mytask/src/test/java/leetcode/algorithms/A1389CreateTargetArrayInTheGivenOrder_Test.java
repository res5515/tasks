package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class A1389CreateTargetArrayInTheGivenOrder_Test {

    @Test
    public void solutionTest() {
        A1389CreateTargetArrayInTheGivenOrder solution = new A1389CreateTargetArrayInTheGivenOrder();
        assertArrayEquals(new int[] {0,4,1,3,2}, solution.createTargetArray(new int[] {0,1,2,3,4}, new int[]{0,1,2,2,1}));
        assertArrayEquals(new int[] {0,1,2,3,4}, solution.createTargetArray(new int[] {1,2,3,4,0}, new int[]{0,1,2,3,0}));
    }
}
