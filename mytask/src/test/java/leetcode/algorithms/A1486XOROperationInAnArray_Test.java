package leetcode.algorithms;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1486XOROperationInAnArray_Test {
    @Test
    public void solutionTest() {
        A1486XOROperationInAnArray solution = new A1486XOROperationInAnArray();
        assertEquals(8, solution.xorOperation(5, 0));
        assertEquals(8, solution.xorOperation(4, 3));
    }

}
