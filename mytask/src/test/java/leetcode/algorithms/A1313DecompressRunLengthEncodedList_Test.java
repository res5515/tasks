package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1313DecompressRunLengthEncodedList_Test {
    @Test
    public void solutionTest() {
        A1313DecompressRunLengthEncodedList solution = new A1313DecompressRunLengthEncodedList();
        assertArrayEquals(new int[] {2,4,4,4}, solution.decompressRLElist(new int[]{1,2,3,4}));
    }
}
