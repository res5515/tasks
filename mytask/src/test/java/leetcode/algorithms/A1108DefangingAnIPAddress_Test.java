package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1108DefangingAnIPAddress_Test {
    @Test
    public void solutionTest() {
        A1108DefangingAnIPAddress a1108DefangingAnIPAddress = new A1108DefangingAnIPAddress();
        assertEquals("255[.]100[.]50[.]0", a1108DefangingAnIPAddress.defangIPaddr("255.100.50.0"));
    }
}
