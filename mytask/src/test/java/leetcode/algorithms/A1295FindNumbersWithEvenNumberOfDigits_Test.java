package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1295FindNumbersWithEvenNumberOfDigits_Test {

    @Test
    public void solutionTest() {
        A1295FindNumbersWithEvenNumberOfDigits solution = new A1295FindNumbersWithEvenNumberOfDigits();
        assertEquals(2, solution.findNumbers(new int[]{12,345,2,6,7896}));
        assertEquals(1, solution.findNumbers(new int[]{555,901,482,1771}));
    }
}
