package leetcode.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class A1480RunningSumof1dArray_Test {
    @Test
    public void test() {
        A1480RunningSumof1dArray solution = new A1480RunningSumof1dArray();
        int[] arr1 = {1,2,3,4};
        int[] arr2 = {1,1,1,1,1};
        int[] arr3 = {3,1,2,10,1};

        int[] expected1 = {1,3,6,10};
        int[] expected2 = {1,2,3,4,5};
        int[] expected3 = {3,4,6,16,17};
        Assertions.assertArrayEquals(expected1, solution.runningSum(arr1));
        Assertions.assertArrayEquals(expected2, solution.runningSum(arr2));
        Assertions.assertArrayEquals(expected3, solution.runningSum(arr3));
    }
}
