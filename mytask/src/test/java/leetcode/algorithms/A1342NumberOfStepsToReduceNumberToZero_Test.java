package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1342NumberOfStepsToReduceNumberToZero_Test {
    @Test
    public void solutionTest() {
        A1342NumberOfStepsToReduceNumberToZero a1342NumberOfStepsToReduceNumberToZero = new A1342NumberOfStepsToReduceNumberToZero();
        assertEquals(6, a1342NumberOfStepsToReduceNumberToZero.numberOfSteps(14));
    }
}
