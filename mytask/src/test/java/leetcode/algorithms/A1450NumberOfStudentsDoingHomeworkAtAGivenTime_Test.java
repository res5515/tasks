package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1450NumberOfStudentsDoingHomeworkAtAGivenTime_Test {
    @Test
    public void solutionTest() {
        A1450NumberOfStudentsDoingHomeworkAtAGivenTime solution = new A1450NumberOfStudentsDoingHomeworkAtAGivenTime();
        assertEquals(1, solution.busyStudent(new int[]{1,2,3}, new int[]{3,2,7}, 4));
    }
}
