package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1528ShuffleString_Test {

    @Test
    public void solutionTest() {
        A1528ShuffleString a1528ShuffleString = new A1528ShuffleString();
        assertEquals("leetcode", a1528ShuffleString.restoreString("codeleet", new int[]{4,5,6,7,0,2,1,3}));
    }
}
