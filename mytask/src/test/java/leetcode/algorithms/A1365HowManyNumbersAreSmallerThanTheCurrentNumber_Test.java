package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1365HowManyNumbersAreSmallerThanTheCurrentNumber_Test {
    @Test
    public void solutionTest() {
        A1365HowManyNumbersAreSmallerThanTheCurrentNumber solution = new A1365HowManyNumbersAreSmallerThanTheCurrentNumber();
        assertArrayEquals(new int[]{4,0,1,1,3}, solution.smallerNumbersThanCurrent(new int[]{8,1,2,2,3}));
    }
}
