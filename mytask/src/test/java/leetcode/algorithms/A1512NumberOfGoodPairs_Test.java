package leetcode.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class A1512NumberOfGoodPairs_Test {
    @Test
    public void solutionTest() {
        A1512NumberOfGoodPairs a1512NumberOfGoodPairs = new A1512NumberOfGoodPairs();
        assertEquals(7, a1512NumberOfGoodPairs.numIdenticalPairs(new int[]{1,2,3,1,1,1,3}));
    }
}
