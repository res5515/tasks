package leetcode.algorithms.A14LongestComonPrefix;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SolutionTest {
    @Test
    public void testSolution() {
        String result = Solution2.longestCommonPrefix(new String[]{"ca", "a"});
        assertEquals("", result);

        String result2 = Solution2.longestCommonPrefix(new String[]{"flower","flow","flight"});
        assertEquals("fl", result2);

        String result4 = Solution4.longestCommonPrefix(new String[]{"ca", "a"});
        assertEquals("", result4);

        String result4_2 = Solution4.longestCommonPrefix(new String[]{"flower","flow","flight"});
        assertEquals("fl", result4_2);
    }
}
