package leetcode.algorithms.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MathUtilsTest {
    @Test
    public void expandNumberTest() {
        Assertions.assertArrayEquals(new Integer[]{2}, MathUtils.expandNumber(2));
        Assertions.assertArrayEquals(new Integer[]{2,3}, MathUtils.expandNumber(23));
        Assertions.assertArrayEquals(new Integer[]{2,3,4}, MathUtils.expandNumber(234));
    }
}
