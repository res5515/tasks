package interview.nitka;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SolutionTest {
    @Test
    public void e() {
        Solution solution = new Solution();
        assertEquals(0.0, solution.w(0,0));
        assertEquals(0.5, solution.w(1,0));
        assertEquals(0.5, solution.w(1,1));
        assertEquals(0.75, solution.w(2,0));
        assertEquals(1.5, solution.w(2,1));
        assertEquals(0.75, solution.w(2,2));
        assertEquals(0.875, solution.w(3,0));
        assertEquals(2.125, solution.w(3,1));
        assertEquals(2.125, solution.w(3,2));
        assertEquals(0.875, solution.w(3,3));
        assertEquals(306.48749781747574, solution.w(322,156));
//
    }
}
